from sagemaker.predictor import RealTimePredictor
from sagemaker.predictor import json_serializer
from sagemaker.predictor import csv_serializer
import numpy as np
import pandas as pd

# get the name in SageMaker interface and put it there
endpoint = 'sagemaker-test-2018-09-26-17-04-49-818'

# you can use other serializer, or none if you are able
# to get data in binary format
predictor = RealTimePredictor(endpoint, serializer=csv_serializer)

# and call prediction as previously
shape=pd.read_csv("iris.csv", header=None)

import itertools

a = [50*i for i in range(3)]
b = [40+i for i in range(10)]
indices = [i+j for i,j in itertools.product(a,b)]

test_data=shape.iloc[indices[:-1]]
test_X=test_data.iloc[:,1:]
test_y=test_data.iloc[:,0]

res = predictor.predict(test_X.values)
print(res.decode('utf-8'))