import json
import io
import csv
import boto3
import os

try:
    from urlparse import urlparse, parse_qs
except ImportError:
    from urllib.parse import urlparse, parse_qs

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

sagemaker= boto3.client('runtime.sagemaker')

def lambda_handler(event, context):
    print ("event=", " type=", type(event), event)
    for k, v in event.items():
        print("k=",k," v=",v)
    try:
        my_dict = {k:float(v[0]) for k, v in event.iteritems()}
    except AttributeError:
        my_dict = {k:float(v[0]) for k, v in event.items()}
    print("my_dict=", my_dict)
    print("my_dict.keys()=",my_dict.keys())
    f = StringIO()
    w = csv.DictWriter(f, my_dict.keys())
    #w.writeheader()
    w.writerow(my_dict)
    response = sagemaker.invoke_endpoint(
                    EndpointName='decision-trees-sample-2018-09-28-18-38-16-504',
                    Body=f.getvalue(),
                    ContentType='text/csv',
                    Accept='Accept'
                )
    print("response=", response)
    stream = io.BufferedReader(response['Body']._raw_stream)
    			    prediction = str(stream.read().decode('utf-8'))
    			    print("ANSWER=",prediction)
    return prediction
