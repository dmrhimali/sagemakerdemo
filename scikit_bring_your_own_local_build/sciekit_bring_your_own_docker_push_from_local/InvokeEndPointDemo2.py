import boto3
import itertools
from sagemaker.predictor import RealTimePredictor
from sagemaker.predictor import json_serializer
from sagemaker.predictor import csv_serializer
import numpy as np
import pandas as pd
import json
import io


def np2csv(arr):
    csv = io.BytesIO()
    np.savetxt(csv, arr, delimiter=',', fmt='%g')
    return csv.getvalue().decode().rstrip()


runtime= boto3.client('runtime.sagemaker')

shape=pd.read_csv("iris.csv", header=None)
a = [50*i for i in range(3)]
b = [40+i for i in range(10)]
indices = [i+j for i,j in itertools.product(a,b)]
test_data=shape.iloc[indices[:-1]]
test_X=test_data.iloc[:,1:]
test_y=test_data.iloc[:,0]

payload = payload = np2csv(test_X.values)
print(payload)
endpoint = 'decision-trees-sample-2018-09-28-14-49-16-385'

response  = runtime.invoke_endpoint(
    EndpointName=endpoint,
    ContentType='text/csv',
    Body=payload
)

print (response['Body'])

stream = io.BufferedReader(response['Body']._raw_stream)
print(stream.read())

