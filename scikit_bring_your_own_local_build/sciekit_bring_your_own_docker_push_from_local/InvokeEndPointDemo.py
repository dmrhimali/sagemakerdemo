import boto3
import json
import itertools
import numpy as np
import pandas as pd

'''
import boto3

aws_access_key_id = '...............'
aws_secret_access_key = '................'
tkn = '..........'
region_name = '............'

amz = boto3.client('sagemaker-runtime',
                   aws_access_key_id=aws_access_key_id,
                   aws_secret_access_key=aws_secret_access_key,
                   aws_session_token=tkn,
                   region_name=region_name)
'''

sagemaker = boto3.client('runtime.sagemaker')

shape=pd.read_csv("iris.csv", header=None)
a = [50*i for i in range(3)]
b = [40+i for i in range(10)]
indices = [i+j for i,j in itertools.product(a,b)]
test_data=shape.iloc[indices[:-1]]
test_X=test_data.iloc[:,1:]
test_y=test_data.iloc[:,0]

data = test_X.values

response = sagemaker.invoke_endpoint(
    EndpointName='sagemaker-test-2018-09-26-17-04-49-818',
    ContentType='text/csv',
	Body=json.dumps(data)
)

print(response)
result = json.loads(response['Body'].read().decode())
print(result)