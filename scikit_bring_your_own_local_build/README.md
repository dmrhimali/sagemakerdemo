
#  PART1: Build sagemaker docker image locally

This project builds a sagemaker docker image locally and push it to AWS ECR.

:exclamation: DELETE END POINTS AFTER FINISHING.

## scikit-bring-your-own
Ref: [git: scikit-bring-your-own](https://github.com/awslabs/amazon-sagemaker-examples/tree/master/advanced_functionality/scikit_bring_your_own)

This project has detailed instrctions and a jupytor book on how to create, build and run a docker sagemaker instance in aws cloud.

## scikit-bring-your-own build from local machine
In this project, I am going to create and build the docker image locally in my laptop and push to aws ECR.

### Useful references
[medium: scikit-bring-yourown](https://medium.com/smileinnovation/sagemaker-bring-your-own-algorithms-719dd539607d)

[amazon blogs: building docker container](https://aws.amazon.com/blogs/machine-learning/train-and-host-scikit-learn-models-in-amazon-sagemaker-by-building-a-scikit-docker-container/)


### Download the code from git 
---
https://github.com/awslabs/amazon-sagemaker-examples

`$ git clone https://github.com/awslabs/amazon-sagemaker-examples.git`

### Copy scikit-bring-your-own project to your own folder

```bash
RBH12282:sagemaker rdissanayakam$ ls
amazon-sagemaker-examples   mySagemakerLocal
RBH12282:sagemaker rdissanayakam$ cd mySagemakerLocal/
RBH12282:mySagemakerLocal rdissanayakam$ ls
scikit_bring_your_own
RBH12282:mySagemakerLocal rdissanayakam$ pwd
/Users/rdissanayakam/COURSES/ML/sagemaker/mySagemakerLocal
RBH12282:mySagemakerLocal rdissanayakam$
```

This will act as the git project we created in our laptop.

### Build the docker image locally
---
commands:

`$ docker build -t sagemaker-test .`

sample run:

```bash
RBH12282:container rdissanayakam$ docker build -t sagemaker-test .
Sending build context to Docker daemon  66.05kB
Step 1/9 : FROM ubuntu:16.04
16.04: Pulling from library/ubuntu
Status: Downloaded newer image for ubuntu:16.04
---> b9e15a5d1e1a
Step 2/9 : MAINTAINER Amazon AI <sage-learner@amazon.com>
.
.
Step 9/9 : WORKDIR /opt/program
---> Running in 549b2bc4a0dc
Removing intermediate container 549b2bc4a0dc
---> df7efe1992bf
Successfully built df7efe1992bf
Successfully tagged sagemaker-test:latest
```

Now you should see the docker image:

```bash
RBH12282:container rdissanayakam$ docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
sagemaker-test       latest              df7efe1992bf        25 seconds ago      430MB
```

### Test the docker image locally
---
**Train:**

commands:

`$ docker run --rm -v $(pwd)/local_test/test_dir:/opt/ml sagemaker-test train`

sample run:

```bash
RBH12282:container rdissanayakam$ ls
Dockerfile		ReadMe.md		build_and_push.sh	decision_trees		local_test
RBH12282:container rdissanayakam$ docker run --rm -v $(pwd)/local_test/test_dir:/opt/ml sagemaker-test train
Starting the training.
Training complete.
```
## test
**Test:**

commands:
`docker run --rm -v $(pwd)/local_test/test_dir:/opt/ml -p 8080:8080 sagemaker-test serve`

sample run:

```bash
RBH12282:container rdissanayakam$ docker run --rm -v $(pwd)/local_test/test_dir:/opt/ml -p 8080:8080 sagemaker-test serve
Starting the inference server with 4 workers.
[2018-09-26 15:12:34 +0000] [11] [INFO] Starting gunicorn 19.9.0
[2018-09-26 15:12:34 +0000] [11] [INFO] Listening at: unix:/tmp/gunicorn.sock (11)
[2018-09-26 15:12:34 +0000] [11] [INFO] Using worker: gevent
[2018-09-26 15:12:34 +0000] [16] [INFO] Booting worker with pid: 16
[2018-09-26 15:12:34 +0000] [17] [INFO] Booting worker with pid: 17
[2018-09-26 15:12:34 +0000] [18] [INFO] Booting worker with pid: 18
[2018-09-26 15:12:34 +0000] [19] [INFO] Booting worker with pid: 19
172.17.0.1 - - [26/Sep/2018:15:12:57 +0000] "GET /ping HTTP/1.1" 200 1 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36"
172.17.0.1 - - [26/Sep/2018:15:13:48 +0000] "GET /ping HTTP/1.1" 200 1 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36"
```

**Verify with Chrome Postman:**

![Postman](img/local_docker_postman.png " Postman verification")

### Build image locally and push to sagemaker-ecr
---

#### install aws cli: https://docs.aws.amazon.com/cli/latest/userguide/cli-install-macos.html

commands:

```bash
$ curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"

$ sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

$ aws --version
```

```bash
RBH12282:mySagemakerLocal rdissanayakam$ curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                Dload  Upload   Total   Spent    Left  Speed
100 10.1M  100 10.1M    0     0  4300k      0  0:00:02  0:00:02 --:--:-- 4299k
RBH12282:mySagemakerLocal rdissanayakam$ unzip awscli-bundle.zip
Archive:  awscli-bundle.zip
inflating: awscli-bundle/install   
inflating: awscli-bundle/packages/docutils-0.14.tar.gz  
inflating: awscli-bundle/packages/python-dateutil-2.6.1.tar.gz  
...
You can now run: /usr/local/bin/aws --version
```

Verify installation:
```bash
RBH12282:mySagemakerLocal rdissanayakam$ aws --version
aws-cli/1.16.21 Python/3.6.5 Darwin/17.7.0 botocore/1.12.11
```

#### Create a access-key  in aws console:

Under aws.console webpage myusername>security credentials: create a new access-key-security key pair (can be downloaded at the time of creation - not shown below) and note it down:
![AccessKey](img/aws_access_key.png " AWS Access Key")

#### Configure aws cli to use access key:

```bash
RBH12282:container rdissanayakam$ aws configure
AWS Access Key ID [****************SY5Q]: AKIAIRXMFIIOAF6AKYIA
AWS Secret Access Key [****************wzns]: <YOUR SECRET KEY>
Default region name [None]: us-east-1
Default output format [None]: 
```

#### Run build to push build and push docker image to ecr

```bash
RBH12282:container rdissanayakam$ ./build_and_push.sh sagemaker-test
WARNING! Using --password via the CLI is insecure. Use --password-stdin.
Login Succeeded
Sending build context to Docker daemon  66.05kB
Step 1/9 : FROM ubuntu:16.04
. . .
Step 9/9 : WORKDIR /opt/program
Successfully built df7efe1992bf
Successfully tagged sagemaker-test:latest
The push refers to repository [545200963109.dkr.ecr.us-east-1.amazonaws.com/sagemaker-test]
9eed6d8db1e3: Pushed 
. . .
latest: digest: sha256:20c2bdadfbc6119fd5f43f4dfd4c29df3212413800817760631a49209f26991d size: 1989
RBH12282:container rdissanayakam$ 
```

### Train and deploy model endpoints using sagemaker jupitor notebook
---
    - 
#### Create, start and open a sagemaker notebook instance :
![SagemakerNotebook](img/sagemaker_notebook.png " Sagemaker Notebook")

#### Inside that create a data folder and upload training data (iris.csv) there
![TrainDaata](img/train_data.png " Train Data Upload")
    
#### Inside the same folder Create your jupytor notebook(local_docker_to_ecr.ipynb) as shown in above image

#### In jupytor notebook run following code(run code in local_docker_to_ecr.ipynb):
        
**setup**

```python
import sagemaker as sage
import boto3
from sagemaker import get_execution_role

role = get_execution_role()
sess = sage.Session()

account = sess.boto_session.client('sts').get_caller_identity()['Account']
region = sess.boto_session.region_name # or setup the region by hand

# the image you pushed
image = '{}.dkr.ecr.{}.amazonaws.com/sagemaker-test'.format(account, region)
```

**upload training data**
```python
# S3 prefix, where you uploaded your training data
WORK_DIRECTORY = 'data'
prefix='trainingdata'
data_location = sess.upload_data(WORK_DIRECTORY, key_prefix=prefix)
#data_location = 's3://{}/sagemaker-test'.format(sess.default_bucket())
#output:INFO:sagemaker:Created S3 bucket: sagemaker-us-east-1-545200963109

tree = sage.estimator.Estimator(image,
                role, 1, 'ml.c4.2xlarge',
                output_path="s3://{}/output".format(sess.default_bucket()),
                sagemaker_session=sess)
```

**train**

```python
tree.fit(data_location)
#output: INFO:sagemaker:Creating training-job with name: sagemaker-test-2018-09-26-17-48-08-095
```

**deploy**

```python
#predictor = tree.deploy(initial_instance_count=1,instance_type='ml.m4.xlarge')

from sagemaker.predictor import csv_serializer
predictor = tree.deploy(1, 'ml.m4.xlarge', serializer=csv_serializer)
```

**predict**

```python
import numpy as np
import pandas as pd
shape=pd.read_csv("data/iris.csv", header=None)

import itertools

a = [50*i for i in range(3)]
b = [40+i for i in range(10)]
indices = [i+j for i,j in itertools.product(a,b)]

test_data=shape.iloc[indices[:-1]]
test_X=test_data.iloc[:,1:]
test_y=test_data.iloc[:,0]

print(predictor.predict(test_X.values).decode('utf-8'))

#delete end point
sess.delete_endpoint(predictor.endpoint)
```


**output**
```
setosa
setosa
setosa
setosa
setosa
setosa
setosa
setosa
setosa
setosa
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
virginica
virginica
virginica
virginica
virginica
virginica
virginica
virginica
virginica
```

# Using Api outside noetebook (call endpoints from your laptop programmatically)

### Make sure Created endpoint is in service (you need to delete this later in jupytor notebook after this test): Copy endpoint name
![endpoint](img/endpoint.png " endppoint")

### In your local machine install sagemaker python library
`$ pip install sagemaker`

### Method1: realTimePredictor.py
Create following python file in your laptop (the iris.csv should be in same folder for us to generate some test data):
```
iris.csv
realTimePredictorDemo.py
```

![files](img/files.png " files")

Run python file:

```bash
RBH12282:mySagemakerLocal rdissanayakam$ python realTimePredictorDemo.py 
setosa
setosa
setosa
setosa
setosa
setosa
setosa
setosa
setosa
setosa
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
versicolor
virginica
virginica
virginica
virginica
virginica
virginica
virginica
virginica
virginica
```


### Method2: InvokePointDemo2.py
should give same output
![file2](img/file2out.png " file2")

## Delete endpoint

MAKE SURE TO DELETE ENDPOINT once done: (or use aws console to do this)

```python
sess.delete_endpoint(predictor.endpoint)
```


# PART2: Setup API Gateway rest endpoints

## Verify that your endpoint is running

![ep2](img/endpoint2.png " ep2")

## Setup a lambda function

https://aws.amazon.com/blogs/machine-learning/call-an-amazon-sagemaker-model-endpoint-using-amazon-api-gateway-and-aws-lambda/

![ep2](img/iamrole.png " ep2")

Code to be pasted to lambda function is in myLambdaFunction.py:

![ep2](img/lambda.png " ep2")

![ep2](img/env.png " ep2")

## Test the lambda function by creating a test

![ep2](img/test.png " ep2")

If successful: view details in top:

![ep2](img/testresult.png " ep2")

## Create Api:

Create api (API Gateway -> Create api):
![ep2](img/createapi.png " ep2")

Create resource (Actions -> Create resource):
![ep2](img/createresource.png " ep2")

Create post method (Create method -> post):
![ep2](img/post.png " ep2")

Lambda function in aws console now shows api gateway trigger:
![ep2](img/trigger.png " ep2")

## Test Api:
![ep2](img/testapi.png " ep2")

## Deploy Api:
![ep2](img/deploy.png " ep2")

Note the invoke url:
![ep2](img/invokeurl.png " ep2")

Now your api is publicly avaiable to you.

## Test with Postman API:
![ep2](img/postman1.png " ep2")

## Distribute api keys
For consumers to use this, we need to create api key per customer:

Setup api key:
 
Click method request link:
![ep2](img/clickmethodrequestlink.png " ep2")

Associate api key with a usage plan:
![ep2](img/associateapikey.png " ep2")	 

Create usage plan for api key:
![ep2](img/createusageplan.png " ep2")

Add api stage:
![ep2](img/addapistage.png " ep2")


Add API Key to usage plan:
![ep2](img/addapikey.png " ep2")

Click ‘done’.
![ep2](img/usageplan.png " ep2")

Now API key holder can access the rest api endpoint using just api key (no authorize section):
![ep2](img/apikeypostman.png " ep2")